﻿using System.Runtime.CompilerServices;
using System.Text;

namespace TimeStruct
{
    #pragma warning disable SA1600
    public struct Time
    {
        public Time(int minutes)
            : this(0, minutes)
        {
        }

        public Time(int hours, int minutes)
        {
            int addHour;
            double mm = minutes;
            double hh = hours;

            if (mm >= 0)
            {
                if (mm == 60)
                {
                    hh++;
                    mm = 0;
                }
                else if (mm > 60)
                {
                    addHour = (int)Math.Truncate((decimal)(mm / 60));
                    mm = (int)Math.Round((decimal)((mm / 60) - addHour) * 60);
                    hh += addHour;
                }
            }

            if (mm < 0)
            {
                if (mm == -60)
                {
                    hh--;
                    mm = 0;
                }
                else if (mm < -60)
                {
                    addHour = (int)Math.Truncate((decimal)(mm / 60));
                    mm = 60 + Math.Round(((mm / 60) - addHour) * 60);
                    hh += addHour - 1;
                }
                else if (mm > -60)
                {
                    if (hh == 1 || hh == 0)
                    {
                        hh--;
                    }

                    mm = 60 + mm;
                }
            }

            if (hh >= 0)
            {
                if (hh == 24)
                {
                    hh = 0;
                }
                else if (hh > 24)
                {
                    addHour = (int)Math.Truncate((decimal)(hh / 24));
                    hh = (int)Math.Round((decimal)((hh / 24) - addHour) * 24);
                }
            }

            if (hh < 0)
            {
                if (hh == -24)
                {
                    hh = 0;
                }
                else if (hh < -24)
                {
                    addHour = (int)Math.Truncate((decimal)(hh / 24));
                    hh = 24 + (int)Math.Round((decimal)((hh / 24) - addHour) * 24);
                }
                else if (hh > -24)
                {
                    hh = 24 + hh;
                }
            }

            this.Minutes = (int)mm;
            this.Hours = (int)hh;
        }

        public int Hours { get; }

        public int Minutes { get; }

        public new string? ToString()
        {
            var sb = new StringBuilder();

            if (this.Hours < 10)
            {
                sb.Append("0" + this.Hours + ":");
            }
            else
            {
                sb.Append(this.Hours + ":");
            }

            if (this.Minutes < 10)
            {
                sb.Append("0" + this.Minutes);
            }
            else
            {
                sb.Append(this.Minutes);
            }

            return sb.ToString();
        }

        public void Deconstruct(out int expectedHours, out int expectedMinutes)
        {
            expectedHours = this.Hours;
            expectedMinutes = this.Minutes;
        }
    }
}
